pipeline {
    environment {
        REGISTRY_URL = "http://harbor2.coe.com/"
		SONARQUBE_PROJECT_TITLE = "ShopizerAppFinal"
        REGISTRY_NAME = "harbor2.coe.com/pipeline/shopizer"
		KUBERNETESserver_URL = "https://lb.coe.com/k8s/clusters/c-v84ch"
        GIT_URL = "http://git.coe.com:3000/rakesh/backupV2.git"
        SONARQUBE_URL = "http://10.0.100.16"
    }
    agent { 
        label 'master'  
    }
    tools{
        maven 'default'    
    }
    stages {
        stage('Git checkout on jenkins master VM') {
            steps {
                git "${env.GIT_URL}"
                sh 'ls -al'
            }
        }
        stage('Code Quality Check via SonarQube') {
            steps {
                script {
					def jobBaseName = "${env.JOB_NAME}".split('/').last()
                    def scannerHome = tool 'sonar-scanner';
                    withSonarQubeEnv("sonarqube") {
                        sh "${tool("sonar-scanner")}/bin/sonar-scanner \
                        -Dsonar.projectKey=$SONARQUBE_PROJECT_TITLE \
                        -Dsonar.java.binaries=$WORKSPACE \
                        -Dsonar.host.url='$SONARQUBE_URL' "
                    }
                }
            }
        }
        stage('Maven_Build') {
            steps {
                sh 'echo $JAVA_HOME'
                sh 'mvn --version'
                sh 'mvn clean install'
            }
        }
        stage('Application_ContainerImage_Build_&_Push_Harbor') {
            steps {
                script {
                  docker.withRegistry(REGISTRY_URL , 'harbor') {
                    sh "docker build -t $REGISTRY_NAME:$BUILD_NUMBER ./sm-shop/"
                    sh "docker push $REGISTRY_NAME:$BUILD_NUMBER"
					} 
                }
            }
        }
        stage('Deploy App') {
            steps {
                script {
                    withKubeConfig([credentialsId: 'APILoginRkUser', serverUrl: 'https://lb.coe.com/k8s/clusters/c-v84ch']) {
                        sh 'kubectl version --short'
                        sh "BUILD_NUMBER=${BUILD_NUMBER} && envsubst < sm-shop-deploy.yaml > shop.yaml"
                        sh 'kubectl apply -f shop.yaml'
                    }
                }
            }
        }
        stage ('Cucumber Test Reports') {
            steps {
				sh 'chmod +x $WORKSPACE/CucumberSeleniumTest/src/test/java/com/cucumberseleniumdemo/chromedriver'
                sh 'mvn -f ./CucumberSeleniumTest/pom.xml clean install'
                cucumber fileIncludePattern: "**/*.json",
				jsonReportDirectory: './CucumberSeleniumTest'
            }
        }
    }
}
